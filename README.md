## Visit my article on mimiz.fr

This git repository contains source code for my article on how to deploy a docker registry on Kubernetes.

You can read it on my blog : [Deploy a docker registry on bare metal (debian) kubernetes](https://www.mimiz.fr/docker-registry-kubernetes.html)

